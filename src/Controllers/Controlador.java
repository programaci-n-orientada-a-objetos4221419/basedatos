
package Controllers;

import Modelo.dbProducto;
import Modelo.Productos;
import Views.dlgProducto;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Date;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sound.sampled.Port;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JDialog;
import javax.swing.JOptionPane;


public class Controlador implements ActionListener {
    dbProducto db;
    Productos pro;
    dlgProducto vista;
    boolean act;
    boolean hab;
    boolean ena;

    public Controlador(dbProducto db,Productos pro, dlgProducto vista) {
        this.db = db;
        this.pro = pro;
        this.vista = vista;
        
        vista.btnBuscar.addActionListener(this);
        vista.btnCancelar.addActionListener(this);
        vista.btnCerrar.addActionListener(this);
        vista.btnDeshabi.addActionListener(this);
        vista.btnGuardar.addActionListener(this);
        vista.btnLimpiar.addActionListener(this);
        vista.btnNuevo.addActionListener(this);
        vista.btnBuscar1.addActionListener(this);
        vista.btnHabilitar.addActionListener(this);
        vista.btnLimpiarDes.addActionListener(this);
        
        
    }
    
    private void iniciarVista() throws Exception{
        vista.jTableProductos.setModel(db.listar());
        vista.jTableDes.setModel(db.listarDes());
        vista.setTitle(":: Productos ::");
        vista.setSize(620, 570);
        vista.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
         if (e.getSource() == vista.btnNuevo) {
            ena = true;
            vista.txtCodigo.setEnabled(ena);
            vista.txtNombre.setEnabled(ena);
            vista.txtPrecio.setEnabled(ena);
            vista.txtCodigo1.setEnabled(ena);
            vista.txtNombreDes.setEnabled(ena);

            vista.btnBuscar.setEnabled(ena);
            vista.btnDeshabi.setEnabled(ena);
            vista.btnGuardar.setEnabled(ena);
            vista.btnCancelar.setEnabled(ena);
            vista.btnCerrar.setEnabled(ena);
            vista.btnLimpiar.setEnabled(ena);
            vista.btnBuscar1.setEnabled(hab);
            vista.btnHabilitar.setEnabled(hab);
            
            vista.jcFecha.setEnabled(ena);
            vista.jTableProductos.setEnabled(ena);
            vista.jTableDes.setEnabled(ena);
        } 
         else if (e.getSource() == vista.btnGuardar) {
             if(valiEmpty() != true){
                 JOptionPane.showMessageDialog(vista,"No deje ningun espacio en blanco");
             }
             else{
                 try {
                     if (act != true) {
                         pro = (Productos) db.buscar(vista.txtCodigo.getText());
                         if (pro.getCodigo().equals("")) {
                             pro.setCodigo(vista.txtCodigo.getText());
                             pro.setNombre(vista.txtNombre.getText());
                             pro.setPrecio(Float.parseFloat(vista.txtPrecio.getText()));
                             fecha();
                             db.insertar(pro);
                         } else {
                             JOptionPane.showMessageDialog(vista,"Ese codigo ya existe");
                         }
                     } else {
                         pro.setCodigo(vista.txtCodigo.getText());
                         pro.setNombre(vista.txtNombre.getText());
                         pro.setPrecio(Float.parseFloat(vista.txtPrecio.getText()));
                         fecha();
                         db.actualizar(pro);
                         act = false;
                         String urlImagen = "F:/Iconos/Guardar.png";
                         ImageIcon img = new ImageIcon(urlImagen);
                         Icon Actualizar = new ImageIcon(img.getImage());
                         vista.btnGuardar.setIcon(Actualizar);
                     }
                     vista.jTableProductos.setModel(db.listar());
                     limpiar();
                 } catch (Exception ex) {
                     JOptionPane.showMessageDialog(vista,"Surgio un error al guardar: "+ ex.getMessage());
                 }
             }  
        } 
        else if (e.getSource() == vista.btnBuscar) { 
            if(vista.txtCodigo.getText().isEmpty()){
                JOptionPane.showMessageDialog(vista,"Porfavor seleccione el codigo que desea buscar");
            }
            else{
                act = true;
                try {
                    pro = (Productos) db.buscar(vista.txtCodigo.getText());
                    vista.txtPrecio.setText(String.valueOf(pro.getPrecio()));
                    vista.txtNombre.setText(pro.getNombre());
                    vista.jcFecha.setDate(Date.valueOf(pro.getFecha()));
                    vista.txtNombre.setText(pro.getNombre());
                    String urlImagen = "F:/Iconos/actualizar.png";
                    ImageIcon img = new ImageIcon(urlImagen);
                    Icon Actualizar = new ImageIcon(img.getImage());
                    vista.btnGuardar.setIcon(Actualizar);

                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(vista, "Surgio un error al buscar los activados: " + ex.getMessage());
                }
            } 
        }
        else if (e.getSource() == vista.btnBuscar1) {
            if(vista.txtCodigo1.getText().isEmpty()){
                JOptionPane.showMessageDialog(vista,"Porfavor seleccione el codigo que desea buscar");
            }
            else{
                hab = true;
                try {
                    pro = (Productos) db.buscarDes(vista.txtCodigo1.getText());
                    vista.txtNombreDes.setText(pro.getNombre());
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(vista, "Surgio un error al buscar los desactivados: " + ex.getMessage());
                }
            } 
        } 
        else if (e.getSource() == vista.btnDeshabi) {
            if(act != true){
                JOptionPane.showMessageDialog(vista,"Porfavor busque un codigo antes de desabilitar");
            }
            else{
                try {
                    db.desabilitar(pro);
                    vista.jTableProductos.setModel(db.listar());
                    vista.jTableDes.setModel(db.listarDes());
                    act = false;
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(vista,"Surgio un error al deshabilitar: "+ ex.getMessage());;
                }
                
            }
        }
        else if (e.getSource() == vista.btnHabilitar) {
            if(hab != true){
                JOptionPane.showMessageDialog(vista,"Porfavor busque un codigo antes de habilitar");
            }
            else{
                try {
                    db.habilitar(pro);
                    vista.jTableDes.setModel(db.listarDes());
                    vista.jTableProductos.setModel(db.listar());
                    hab = false;
                } catch (Exception ex) {
                    Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        } 
        else if (e.getSource() == vista.btnCancelar) {
            ena = false;
            limpiar();
            vista.txtCodigo.setEnabled(ena);
            vista.txtNombre.setEnabled(ena);
            vista.txtPrecio.setEnabled(ena);
            vista.txtCodigo1.setEnabled(ena);
            vista.txtNombreDes.setEnabled(ena);

            vista.btnBuscar.setEnabled(ena);
            //vista.btnDeshabilitar.setEnabled(ena);
            vista.btnGuardar.setEnabled(ena);
            vista.btnCancelar.setEnabled(ena);
            vista.btnCerrar.setEnabled(ena);
            vista.btnLimpiar.setEnabled(ena);
            //vista.btnBuscarDes.setEnabled(hab);
            vista.btnHabilitar.setEnabled(hab);
            
            vista.jcFecha.setEnabled(ena);
            vista.jTableProductos.setEnabled(ena);
            vista.jTableDes.setEnabled(ena);
            
        } 
        else if (e.getSource() == vista.btnCerrar) {
            if (JOptionPane.showConfirmDialog(vista, "Seguro que desea cerrar?", "Cerrar", JOptionPane.YES_NO_OPTION) != JOptionPane.YES_OPTION) {
            } else {
                vista.setVisible(false);
                vista.dispose();
                System.exit(0);
            }
        } 
        else if (e.getSource() == vista.btnLimpiar || e.getSource() == vista.btnLimpiarDes) {
            limpiar();
            act = false;

        }
     }
    public void fecha(){
        int mes = vista.jcFecha.getCalendar().get(Calendar.MONTH) + 1;
        if(mes <10){
            pro.setFecha(String.valueOf(vista.jcFecha.getCalendar().get(Calendar.YEAR)) + ",0"+ mes + ","
                + String.valueOf(vista.jcFecha.getCalendar().get(Calendar.DAY_OF_MONTH)));
        }
        else{
            pro.setFecha(String.valueOf(vista.jcFecha.getCalendar().get(Calendar.YEAR)) + ","+ mes + ","
                + String.valueOf(vista.jcFecha.getCalendar().get(Calendar.DAY_OF_MONTH)));
        }
    }
    
    public void limpiar(){
        vista.txtCodigo1.setText("");
        vista.txtNombreDes.setText("");
        vista.txtCodigo.setText("");
        vista.txtNombre.setText("");
        vista.txtPrecio.setText("");
        vista.jcFecha.setDate(null);
        
    }
    
    public boolean valiEmpty(){
        if(!vista.txtCodigo.getText().isEmpty()
                && !vista.txtNombre.getText().isEmpty()
                && !vista.txtPrecio.getText().isEmpty()
                && vista.jcFecha.getDate() != null){
           return true; 
        }
        else{
            return false;
        }
    }

    public static void main(String[] args) throws Exception {
        dbProducto db = new dbProducto();
        Productos pro = new Productos();
        dlgProducto vista = new dlgProducto();
        Controlador con = new Controlador(db, pro, vista);
        con.iniciarVista();
        
       
    }
    
    
    
}
